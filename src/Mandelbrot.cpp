#include "Mandelbrot.hpp"
#include <thread>
#include <vector>
#include <fstream>
#include <cmath>
#include <cstdio>

class Mandelbrot::impl
{
public:
    impl();
    void SetThreadsNumber( const size_t& );
    void Run();

private:
    const uint8_t MAX_COLOR_COMPONENT_VALUE { 255 };
    const uint8_t ITERATION_MAX { 200 };

    const uint32_t IX_MAX { 1024 };
    const uint32_t IY_MAX { 1024 };

    const double CX_MIN { -2.5 };
    const double CY_MIN { -2.0 };
    const double CX_MAX { 1.5 };
    const double CY_MAX { 2.0 };
    const double ESCAPE_RADIUS { 2.0 };

    const double ER2 { ESCAPE_RADIUS * ESCAPE_RADIUS };
    const double PIXEL_WIDTH { ( CX_MAX - CX_MIN ) / IX_MAX };
    const double PIXEL_HEIGHT { ( CY_MAX - CY_MIN ) / IY_MAX };

    const char* OUTPUT_FILENAME { "output/out.ppm" };

    struct Color { uint8_t r, g, b; };

    size_t m_threadsNumber;

    std::vector< std::thread > m_threads;
    std::vector< Color > m_result;
};

Mandelbrot::impl::impl()
    :
    m_result( IX_MAX * IY_MAX )
{

}

void Mandelbrot::impl::Run()
{
    auto evaluate = [ this ] ( const uint32_t& iyBegin,
                         const uint32_t& iyEnd,
                         const Color& color )
    {
        for ( uint32_t iy = iyBegin; iy < iyEnd; ++iy )
        {
            double cy = CY_MIN + iy * PIXEL_HEIGHT;
            if ( std::fabs( cy ) < PIXEL_HEIGHT / 2 )
                cy = .0;

            for ( uint32_t ix = 0U; ix < IX_MAX; ++ix )
            {
                double cx = CX_MIN + ix * PIXEL_WIDTH;

                double zx = .0;
                double zy = .0;
                double zx2 = zx * zx;
                double zy2 = zy * zy;

                int iteration = 0;

                while ( iteration < ITERATION_MAX && zx2+zy2 < ER2 )
                {
                    zy = 2*zx*zy + cy;
                    zx = zx2 - zy2 + cx;
                    zx2 = zx*zx;
                    zy2 = zy*zy;

                    ++iteration;
                }

                m_result[ IX_MAX * iy + ix ] =
                     iteration == ITERATION_MAX ? color : Color { 255, 255, 255 };
            }
        }
    };

    FILE* fp = fopen( OUTPUT_FILENAME, "wb");
    fprintf( fp,"P6\n %s\n %d\n %d\n %d\n","", IX_MAX, IY_MAX, MAX_COLOR_COMPONENT_VALUE );


    const uint8_t step = IY_MAX / m_threadsNumber;
    const uint8_t colorStep = 255 / m_threadsNumber;

    for ( uint8_t i = 0; i < m_threadsNumber; ++i )
        m_threads.emplace_back(
            [=] { evaluate( i*step, ( i+1 )*step, { i*colorStep, i*colorStep, i*colorStep } ); }
        );

    for ( auto& i : m_threads )
        i.join();

    for ( auto& i : m_result )
    {
        uint8_t colors[] = { i.r, i.g, i.b };
        fwrite( colors, 1, sizeof( colors ), fp );
    }
    fclose( fp );

    puts( "Finished." );
}

void Mandelbrot::impl::SetThreadsNumber( const size_t& n )
{
    m_threadsNumber = n;
}

Mandelbrot::Mandelbrot()
{
    m_pimpl = std::make_unique<impl>();
}

void Mandelbrot::Run()
{
    m_pimpl->Run();
}

Mandelbrot& Mandelbrot::GetInstance()
{
    static Mandelbrot instance;
    return instance;
}

void Mandelbrot::SetThreadsNumber( const size_t& n )
{
    m_pimpl->SetThreadsNumber( n );
}
