#include "Mandelbrot.hpp"

int main( int argc, char** argv )
{
    Mandelbrot::GetInstance().SetThreadsNumber( 16 );
    Mandelbrot::GetInstance().Run();

    return 0;
}
