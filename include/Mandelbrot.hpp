#include <memory>

#ifndef MANDELBROT_HPP
#define MANDELBROT_HPP

class Mandelbrot
{
public:
    static Mandelbrot& GetInstance();
    void SetThreadsNumber( const size_t& );
    void Run();

private:
    Mandelbrot();
    Mandelbrot( const Mandelbrot& ) = delete;
    Mandelbrot operator = ( const Mandelbrot& ) = delete;

    class impl;
    std::unique_ptr< impl > m_pimpl;
};
#endif
