OUT = bin/mandelbrot
CC = g++
IFLAGS = -Iinclude
CFLAGS = -Wall -std=c++14
LDFLAGS = -pthread
SOURCES = src/*

OBJECTS = $(SOURCES:.c=.o)

all:
	mkdir bin output
	$(CC) $(IFLAGS) $(CFLAGS) -o $(OUT) $(SOURCES) $(LDFLAGS)
clean:
	rm -rf bin output
